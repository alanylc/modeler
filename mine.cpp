#include "modelerview.h"
#include "modelerapp.h"
#include "modelerdraw.h"
#include <FL/gl.h>
#include <cmath>

#include "modelerglobals.h"

// inherit off of ModelerView
class MyModel : public ModelerView
{
public:
	MyModel(int x, int y, int w, int h, char *label)
		: ModelerView(x, y, w, h, label) { }

	virtual void draw();
};

// We need to make a creator function, mostly because of
// nasty API stuff that we'd rather stay away from.
ModelerView* createModel(int x, int y, int w, int h, char *label)
{
	return new MyModel(x, y, w, h, label);
}

// helper function
// size of box = 2 * radius
void drawObj(int objType, float radius) {
	if (objType == 0) {
		drawSphere(radius);
	}
	else if (objType == 1) {
		glPushMatrix();
		glTranslated(-radius, -radius, -radius);
		drawBox(radius * 2, radius * 2, radius * 2);
		glPopMatrix();
	}
	else {
		printf("Invalid function paramemter in drawObj(int, float)\n");
	}
}

// We are going to override (is that the right word?) the draw()
// method of ModelerView to draw out SampleModel
void MyModel::draw()
{
	// This call takes care of a lot of the nasty projection 
	// matrix stuff.  Unless you want to fudge directly with the 
	// projection matrix, don't bother with this ...
	ModelerView::draw();

	setAmbientColor(.1f, .1f, .1f);
	// draw the sample model
	setDiffuseColor(COLOR_BLUE);
	glPushMatrix();

		glTranslated(VAL(XPOS), VAL(YPOS) - 0.3, VAL(ZPOS));
		glScaled(0.7, 0.5, 0.7);

		if (VAL(LEVEL) > 0) {
			// body
			setDiffuseColor(VAL(COLOR_BODY_R), VAL(COLOR_BODY_G), VAL(COLOR_BODY_B));
			glPushMatrix();
				glScaled(1.3, 1.5, 1.1);
				drawObj(VAL(OBJ_TYPE), VAL(BODY_SIZE));
			glPopMatrix();
		}

		if (VAL(LEVEL) > 1) {
			// texture box
			glPushMatrix();
			glRotated(VAL(TEXTURE_ROT), 0, 1, 0);
			glTranslated(0.0, 0.0, VAL(BODY_SIZE)*1.1+VAL(TEXTURE_Z)*1);
			glScaled(VAL(TEXTURE_X), VAL(TEXTURE_Y), VAL(TEXTURE_Z));
			drawTextureBox(1,1,VAL(TEXTURE_N), VAL(TEXTURE_LIGHT));
			glPopMatrix();
		}

		glPushMatrix();
		if (VAL(LEVEL) > 2) {
			// head
			setDiffuseColor(VAL(COLOR_HEAD_R), VAL(COLOR_HEAD_G), VAL(COLOR_HEAD_B));
			glTranslated(0, VAL(BODY_SIZE)+VAL(HEAD_SIZE)-0.3, 0);
			glRotated(VAL(ROTATE), 0.0, 1.0, 0.0);
			glScaled(1.2, 1.3, 1.1);
			drawObj(VAL(OBJ_TYPE), VAL(HEAD_SIZE));
		}
		if (VAL(LEVEL) > 3) {
			glPushMatrix();
				// torus
				setDiffuseColor(VAL(TORUS_R), VAL(TORUS_G), VAL(TORUS_B));
				glTranslated(0, VAL(HEAD_SIZE)+0.2, 0);
				glRotated(90, 1.0, 0.0, 0.0);
				glScaled(1.0, 1.0, VAL(TORUS_THICKNESS));
				glNormal3f(0, -1, 0);
				drawTorus(VAL(TORUS_INNER_R), VAL(TORUS_TUBE_R), 1 /*VAL(TORUS_K)*/);
			glPopMatrix();
		}
		if (VAL(LEVEL) > 4) {
			// cone built by triangle primitives
			setDiffuseColor(VAL(COLOR_CONE_R), VAL(COLOR_CONE_G), VAL(COLOR_CONE_B));
			glPushMatrix();
				glTranslated(0, VAL(HEAD_SIZE) + 0.2 + VAL(TORUS_TUBE_R), 0);
				glScaled(0.6, 0.3, 0.6);
				float r = VAL(CONE_R);
				float h = VAL(CONE_H);
				float t = 0.0;
				float phi = M_PI / VAL(CONE_N);
				glLineWidth(4.0);
				glBegin(GL_TRIANGLES);
				for (int i = 0; i < VAL(CONE_N); i++) {
					t = 2 * phi * i;
					float a = r*cos(t - phi);
					float b = r*sin(t - phi);
					float c = r*cos(t + phi);
					float d = r*sin(t + phi);

					glNormal3f(h*d-h*b, a*d-b*c, a*h+c*h);
					glVertex3f(a, 0, b);
					glVertex3f(c, 0, d);
					glVertex3f(0, h, 0);

					glNormal3f(0.0, 1.0f, 0.0);
					glVertex3f(a, 0, b);
					glVertex3f(c, 0, d);
					glVertex3f(0, 0, 0);

					/*
					glNormal3f(0, 1, -1);
					glVertex3f(1, 0, -1);
					glVertex3f(-1, 0, 1);
					glVertex3f(-1, 0, -1);

					glNormal3f(0, 1, -1);
					glVertex3f(1, 0, -1);
					glVertex3f(-1, 0, 1);
					glVertex3f(1, 0, 1);

					glNormal3f(0, 1, -1);
					glVertex3f(-1, 0, 1);
					glVertex3f(-1, 0, -1);
					glVertex3f(0, 1.5, 0);

					glNormal3f(0, 1, -1);
					glVertex3f(1, 0, -1);
					glVertex3f(-1, 0, -1);
					glVertex3f(0, 1.5, 0);

					glNormal3f(0, 1, -1);
					glVertex3f(-1, 0, 1);
					glVertex3f(1, 0, 1);
					glVertex3f(0, 1.5, 0);

					glNormal3f(0, 1, -1);
					glVertex3f(1, 0, -1);
					glVertex3f(1, 0, 1);
					glVertex3f(0, 1.5, 0);
					*/
				}
				glEnd();
				glLineWidth(1.0);
			glPopMatrix();
		}
		/*
		if (VAL(LEVEL) > 3) {
			glTranslated(0, VAL(HEAD_SIZE)*0.111, VAL(HEAD_SIZE)*0.7);
			setDiffuseColor(VAL(COLOR_WHITE);
			glPushMatrix();
				// left eye
				glTranslated(-0.2*VAL(HEAD_SIZE), 0, 0);
				glScaled(VAL(EYE_X), VAL(EYE_Y), VAL(EYE_Z));
				drawObj(VAL(OBJ_TYPE), VAL(EYE_SIZE));
			glPopMatrix();
			glPushMatrix();
				// right eye
				glTranslated(0.2*VAL(HEAD_SIZE), 0, 0);
				glScaled(VAL(EYE_X), VAL(EYE_Y), VAL(EYE_Z));
				drawObj(VAL(OBJ_TYPE), VAL(EYE_SIZE));
			glPopMatrix();
		}
		*/
		glPopMatrix();
		/*
		setDiffuseColor(VAL(COLOR_YELLOW);
		glPushMatrix();
		if (VAL(LEVEL) > 1) {
			// neck ring
			glTranslated(0, 0.87*VAL(HEAD_SIZE), 0);
			glScaled(1.3, 0.3, 1.15);
			drawObj(VAL(OBJ_TYPE), VAL(NECKRING_SIZE));
		}
		glPopMatrix();
		*/

		for (int taili = 0; taili < VAL(TAIL_N); taili++) {
			glPushMatrix();
			if (VAL(TAIL_N) != 1) {
				glRotated(20 * (taili - floor(VAL(TAIL_N)/2)), 0, 1.0, 0);
			}
			if (VAL(LEVEL) > 1) {
				// tail
				setDiffuseColor(VAL(COLOR_TAIL_R), VAL(COLOR_TAIL_G), VAL(COLOR_TAIL_B));
				glTranslated(0, -.667*VAL(BODY_SIZE), -VAL(TAIL_LENGTH) - 0.9*VAL(BODY_SIZE));
				glRotated(180, 0.0, 0.0, 1.0);
				if (VAL(OBJ_TYPE) == 0) {
					drawCylinder(VAL(TAIL_LENGTH), VAL(TAIL_SIZE), VAL(TAIL_SIZE));
				}
				else if (VAL(OBJ_TYPE) == 1) {
					glPushMatrix();
					glRotated(-90, 0.0, 0.0, 1.0);
					drawBox(VAL(TAIL_SIZE), VAL(TAIL_SIZE), VAL(TAIL_LENGTH));
					glPopMatrix();
				}
			}
			if (VAL(LEVEL) > 2) {
				// tail ball
				setDiffuseColor(VAL(COLOR_TAILBALL_R), VAL(COLOR_TAILBALL_G), VAL(COLOR_TAILBALL_B));
				glTranslated(0, 0, -VAL(TAIL_SIZE));
				drawObj(VAL(OBJ_TYPE), VAL(TAIL_SIZE)*2.2);
			}
			if (VAL(LEVEL) > 3) {
				// L-System Tree
				setDiffuseColor(VAL(COLOR_LSYS_R), VAL(COLOR_LSYS_G), VAL(COLOR_LSYS_B));
				glPushMatrix();
				glTranslated(0, 0, -VAL(TAIL_SIZE)*2.2);
				glRotated(-90, 1.0, 0, 0);
				drawTree(VAL(LSYS_N), (float)VAL(LSYS_ANG), VAL(LSYS_LINE), VAL(LSYS_LEAF), VAL(LSYS_R));
				glPopMatrix();
			}
			glPopMatrix();
		}

		glPushMatrix();
		if (VAL(LEVEL) > 1) {
			// left upper arm
			setDiffuseColor(VAL(COLOR_UARM_R), VAL(COLOR_UARM_G), VAL(COLOR_UARM_B));
			glTranslated(-VAL(BODY_SIZE)-0.8*VAL(LEFT_UARM_LEN), 0.54*VAL(BODY_SIZE), 0.0);
			glRotated(90, 0.0, 1.0, 0.0);
			glTranslated(0, 0, VAL(LEFT_UARM_LEN));
			glRotated(VAL(LEFT_ARM_ROT), 0, 1, 0);
			glRotated(VAL(LEFT_UPPER_ANGLE)*VAL(MOVE_LEFT_HAND), 1.0, 0.0, 0.0);
			glTranslated(0, 0, -VAL(LEFT_UARM_LEN));
			drawCylinder(VAL(LEFT_UARM_LEN), VAL(ARM_B), VAL(ARM_A));
		}
		if (VAL(LEVEL) > 2) {
			// knot
			setDiffuseColor(VAL(COLOR_ARMKNOT_R), VAL(COLOR_ARMKNOT_G), VAL(COLOR_ARMKNOT_B));
			drawObj(VAL(OBJ_TYPE), VAL(ARM_B));
		}
		if (VAL(LEVEL) > 3) {
			// left lower arm
			setDiffuseColor(VAL(COLOR_LARM_R), VAL(COLOR_LARM_G), VAL(COLOR_LARM_B));
			glTranslated(0, 0, -VAL(LEFT_LARM_LEN));
			glTranslated(0, 0, VAL(LEFT_LARM_LEN));
			glRotated(VAL(LEFT_LOWER_ANGLE)*VAL(MOVE_LEFT_HAND), 1.0, 0.0, 0.0);
			glTranslated(0, 0, -VAL(LEFT_LARM_LEN));
			drawCylinder(VAL(LEFT_LARM_LEN), VAL(ARM_C), VAL(ARM_B));
		}
		if (VAL(LEVEL) > 4) {
			// left hand
			setDiffuseColor(VAL(COLOR_HAND_R), VAL(COLOR_HAND_G), VAL(COLOR_HAND_B));
			//glTranslated(0, 0, -0.5*VAL(RIGHT_LARM_LEN) + 0.27 / VAL(HAND_SIZE));
			glTranslated(0, 0, -VAL(HAND_SIZE)*0.5);
			drawObj(VAL(OBJ_TYPE), VAL(HAND_SIZE));
		}
		glPopMatrix();

		glPushMatrix();
		if (VAL(LEVEL) > 1) {
			// right upper arm
			setDiffuseColor(VAL(COLOR_UARM_R), VAL(COLOR_UARM_G), VAL(COLOR_UARM_B));
			glTranslated(VAL(BODY_SIZE) + 0.8*VAL(RIGHT_UARM_LEN), 0.54*VAL(BODY_SIZE), 0.0);
			glRotated(-90, 0.0, 1.0, 0.0);
			glTranslated(0, 0, VAL(RIGHT_UARM_LEN));
			glRotated(VAL(RIGHT_ARM_ROT), 0, 1, 0);
			glRotated(VAL(RIGHT_UPPER_ANGLE)*VAL(MOVE_RIGHT_HAND), 1.0, 0.0, 0.0);
			glTranslated(0, 0, -VAL(RIGHT_UARM_LEN));
			drawCylinder(VAL(LEFT_UARM_LEN), VAL(ARM_B), VAL(ARM_A));
		}
		if (VAL(LEVEL) > 2) {
			// knot
			setDiffuseColor(VAL(COLOR_ARMKNOT_R), VAL(COLOR_ARMKNOT_G), VAL(COLOR_ARMKNOT_B));
			drawObj(VAL(OBJ_TYPE), VAL(ARM_B));
		}
		if (VAL(LEVEL) > 3) {
			// right lower arm
			setDiffuseColor(VAL(COLOR_LARM_R), VAL(COLOR_LARM_G), VAL(COLOR_LARM_B));
			glTranslated(0, 0, -VAL(RIGHT_LARM_LEN));
			glTranslated(0, 0, VAL(RIGHT_LARM_LEN));
			glRotated(VAL(RIGHT_LOWER_ANGLE)*VAL(MOVE_RIGHT_HAND), 1.0, 0.0, 0.0);
			glTranslated(0, 0, -VAL(RIGHT_LARM_LEN));
			drawCylinder(VAL(RIGHT_LARM_LEN), VAL(ARM_C), VAL(ARM_B));
		}
		if (VAL(LEVEL) > 4) {
			// right hand
			setDiffuseColor(VAL(COLOR_HAND_R), VAL(COLOR_HAND_G), VAL(COLOR_HAND_B));
			//glTranslated(0, 0, -0.5*VAL(RIGHT_LARM_LEN) + 0.27 / VAL(HAND_SIZE));
			glTranslated(0, 0, -VAL(HAND_SIZE)*0.5);
			drawObj(VAL(OBJ_TYPE), VAL(HAND_SIZE));
		}
		glPopMatrix();

		glPushMatrix();
		if (VAL(LEVEL) > 1) {
			glTranslated(-VAL(BODY_SIZE) - 0.8*VAL(LEFT_ULEG_LEN), -0.86*VAL(BODY_SIZE), 0.0);
			glRotated(90, 0.0, 1.0, 0.0);
			glRotated(VAL(LEFT_LEG_ZH_ROT), 0.0, 0.0, 1.0);
			glTranslated(0, 0, VAL(LEFT_ULEG_LEN));
			// knot
			setDiffuseColor(VAL(COLOR_LEGUKNOT_R), VAL(COLOR_LEGUKNOT_G), VAL(COLOR_LEGUKNOT_B));
			drawObj(VAL(OBJ_TYPE), VAL(LEG_A));
		}
		if (VAL(LEVEL) > 2) {
			glRotated(VAL(LEFT_LEG_HANGLE), 1.0, 0.0, 0.0);
			glTranslated(0, 0, -VAL(LEFT_ULEG_LEN));
			// left upper leg
			setDiffuseColor(VAL(COLOR_ULEG_R), VAL(COLOR_ULEG_G), VAL(COLOR_ULEG_B));
			drawCylinder(VAL(LEFT_ULEG_LEN), VAL(LEG_B), VAL(LEG_A));
		}
		if (VAL(LEVEL) > 3) {
			// knot
			setDiffuseColor(VAL(COLOR_LEGLKNOT_R), VAL(COLOR_LEGLKNOT_G), VAL(COLOR_LEGLKNOT_B));
			drawObj(VAL(OBJ_TYPE), VAL(LEG_B));
		}
		if (VAL(LEVEL) > 4) {
			// left lower leg
			glTranslated(0, 0, -VAL(LEFT_LLEG_LEN));
			glRotated(VAL(LEFT_LEG_ZL_ROT), 0.0, 0.0, 1.0);
			glTranslated(0, 0, VAL(LEFT_LLEG_LEN));
			glRotated(VAL(LEFT_LEG_LANGLE), 1.0, 0.0, 0.0);
			glTranslated(0, 0, -VAL(LEFT_LLEG_LEN));
			setDiffuseColor(VAL(COLOR_LLEG_R), VAL(COLOR_LLEG_G), VAL(COLOR_LLEG_B));
			drawCylinder(VAL(LEFT_LLEG_LEN), VAL(LEG_C), VAL(LEG_B));
		}
		if (VAL(LEVEL) > 5) {
			// left feet
			glTranslated(0, 0, -VAL(FOOT_SIZE)*VAL(FOOT_Z));
			glRotated(VAL(LEFT_FEET_ROT), 0.0, 0.0, 1.0);
			glScaled(VAL(FOOT_X), VAL(FOOT_Y), VAL(FOOT_Z));
			setDiffuseColor(VAL(COLOR_FEET_R), VAL(COLOR_FEET_G), VAL(COLOR_FEET_B));
			drawObj(VAL(OBJ_TYPE), VAL(FOOT_SIZE));
		}
		glPopMatrix();

		glPushMatrix();
		if (VAL(LEVEL) > 1) {
			glTranslated(VAL(BODY_SIZE) + 0.8*VAL(RIGHT_ULEG_LEN), -0.86*VAL(BODY_SIZE), 0.0);
			glRotated(-90, 0.0, 1.0, 0.0);
			glRotated(VAL(RIGHT_LEG_ZH_ROT), 0.0, 0.0, 1.0);
			glTranslated(0, 0, VAL(RIGHT_ULEG_LEN));
			// knot
			setDiffuseColor(VAL(COLOR_LEGUKNOT_R), VAL(COLOR_LEGUKNOT_G), VAL(COLOR_LEGUKNOT_B));
			drawObj(VAL(OBJ_TYPE), VAL(LEG_A));
		}
		if (VAL(LEVEL) > 2) {
			glRotated(VAL(RIGHT_LEG_HANGLE), 1.0, 0.0, 0.0);
			glTranslated(0, 0, -VAL(RIGHT_ULEG_LEN));
			// right upper leg
			setDiffuseColor(VAL(COLOR_ULEG_R), VAL(COLOR_ULEG_G), VAL(COLOR_ULEG_B));
			drawCylinder(VAL(RIGHT_ULEG_LEN), VAL(LEG_B), VAL(LEG_A));
		}
		if (VAL(LEVEL) > 3) {
			// knot
			setDiffuseColor(VAL(COLOR_LEGLKNOT_R), VAL(COLOR_LEGLKNOT_G), VAL(COLOR_LEGLKNOT_B));
			drawObj(VAL(OBJ_TYPE), VAL(LEG_B));
		}
		if (VAL(LEVEL) > 4) {
			// right lower leg
			glTranslated(0, 0, -VAL(RIGHT_LLEG_LEN));
			glRotated(VAL(RIGHT_LEG_ZL_ROT), 0.0, 0.0, 1.0);
			glTranslated(0, 0, VAL(RIGHT_LLEG_LEN));
			glRotated(VAL(RIGHT_LEG_LANGLE), 1.0, 0.0, 0.0);
			glTranslated(0, 0, -VAL(RIGHT_LLEG_LEN));
			setDiffuseColor(VAL(COLOR_LLEG_R), VAL(COLOR_LLEG_G), VAL(COLOR_LLEG_B));
			drawCylinder(VAL(RIGHT_LLEG_LEN), VAL(LEG_C), VAL(LEG_B));
		}
		if (VAL(LEVEL) > 5) {
			// right feet
			glTranslated(0, 0, -VAL(FOOT_SIZE)*VAL(FOOT_Z));
			glRotated(VAL(RIGHT_FEET_ROT), 0.0, 0.0, 1.0);
			glScaled(VAL(FOOT_X), VAL(FOOT_Y), VAL(FOOT_Z));
			setDiffuseColor(VAL(COLOR_FEET_R), VAL(COLOR_FEET_G), VAL(COLOR_FEET_B));
			drawObj(VAL(OBJ_TYPE), VAL(FOOT_SIZE));
		}
		glPopMatrix();

	glPopMatrix();
}

int main()
{
	// Initialize the controls
	// Constructor is ModelerControl(name, minimumvalue, maximumvalue, stepsize, defaultvalue)
	ModelerControl controls[NUMCONTROLS];

	controls[SHININESS] = ModelerControl("Shininess", 10, 30, 1, 20);

	controls[LIGHT_X] = ModelerControl("Light Source X", -10, 10, 1, -10);
	controls[LIGHT_Y] = ModelerControl("Light Source Y", -10, 10, 1, 2);
	controls[LIGHT_Z] = ModelerControl("Light Source Z", -10, 10, 1, 2);
	//controls[LIGHT_ALPHA] = ModelerControl("Light Source Alpha", 0.0f, 1.0f, 0.01f, 0.0f);

	controls[XPOS] = ModelerControl("X Position", -5, 5, 0.01f, 0);
	controls[YPOS] = ModelerControl("Y Position", -5, 5, 0.01f, 0);
	controls[ZPOS] = ModelerControl("Z Position", -5, 5, 0.01f, 0);
	controls[ROTATE] = ModelerControl("Rotate Head", -135, 135, 1, 20);
	
	//controls[EYE_X] = ModelerControl("Scale Eye X", 0.8f, 1.6f, 0.01f, 1.6f);
	//controls[EYE_Y] = ModelerControl("Scale Eye Y", 0.8f, 1.6f, 0.01f, 0.8f);
	//controls[EYE_Z] = ModelerControl("Scale Eye Z", 0.8f, 1.6f, 0.01f, 0.8f);
	//controls[EYE_SIZE] = ModelerControl("Eye Size", 0.6f, 0.8f, 0.01f, 0.6f);

	controls[FOOT_X] = ModelerControl("Scale Foot Z", 0.5f, 2.0f, 0.01f, 1.5f);
	controls[FOOT_Y] = ModelerControl("Scale Foot X", 0.5f, 2.0f, 0.01f, 1.0f);
	controls[FOOT_Z] = ModelerControl("Scale Foot Y", 0.5f, 2.0f, 0.01f, 1.0f);

	controls[OBJ_TYPE] = ModelerControl("Object Type", 0, 1, 1, 0);

	controls[CONE_N] = ModelerControl("Hat Faces", 3, 8, 1, 3);
	controls[CONE_R] = ModelerControl("Hat Radius", 1.5f, 2.6f, 0.01f, 2.0f);
	controls[CONE_H] = ModelerControl("Hat Height", 1.5f, 2.6f, 0.01f, 2.5f);

	controls[LEVEL] = ModelerControl("Level of Details", 0, 6, 1, 6);
	controls[FOOT_SIZE] = ModelerControl("Foot Size", 0.7f, 1.1f, 0.01f, 0.8f);
	controls[TAIL_N] = ModelerControl("Tail Number", 1, 3, 1, 1);
	controls[TAIL_SIZE] = ModelerControl("Tail Size", 0.1f, 0.3f, 0.01f, 0.2f);
	controls[TAIL_LENGTH] = ModelerControl("Tail Length", 0.4f, 1.6f, 0.01f, 1.1f);

	controls[BODY_SIZE] = ModelerControl("Body Size", 1.3f, 1.8f, 0.01f, 1.5f);
	controls[HEAD_SIZE] = ModelerControl("Head Size", 1.6f, 1.8f, 0.01f, 1.8f);
	controls[HAND_SIZE] = ModelerControl("Hand Size", 0.4f, 0.8f, 0.01f, 0.6f);
	//controls[NECKRING_SIZE] = ModelerControl("Neck Ring Size", 1.4f, 1.6f, 0.01f, 1.5f);

	controls[LEFT_UARM_LEN] = ModelerControl("Left Upper Arm Length", 1.3f, 2.5f, 0.01f, 1.5f);
	controls[LEFT_LARM_LEN] = ModelerControl("Left Lower Arm Length", 1.3f, 1.7f, 0.01f, 1.5f);
	controls[RIGHT_UARM_LEN] = ModelerControl("Right Upper Arm Length", 1.3f, 2.5f, 0.01f, 1.5f);
	controls[RIGHT_LARM_LEN] = ModelerControl("Right Lower Arm Length", 1.3f, 1.7f, 0.01f, 1.5f);

	controls[ARM_A] = ModelerControl("Arm Radius A", 0.45f, 0.75f, 0.01f, 0.6f);
	controls[ARM_B] = ModelerControl("Arm Radius B", 0.45f, 0.75f, 0.01f, 0.6f);
	controls[ARM_C] = ModelerControl("Arm Radius C", 0.45f, 0.75f, 0.01f, 0.6f);

	controls[LEFT_ARM_ROT] = ModelerControl("Left Arm Z", -90.0f, 90.0f, 0.01f, 0.0f);
	controls[RIGHT_ARM_ROT] = ModelerControl("Right Arm Z", -90.0f, 90.0f, 0.01f, 0.0f);

	controls[LEFT_ULEG_LEN] = ModelerControl("Left Upper Leg Length", 1.3f, 2.5f, 0.01f, 1.5f);
	controls[LEFT_LLEG_LEN] = ModelerControl("Left Lower Leg Length", 1.3f, 2.0f, 0.01f, 1.5f);
	controls[RIGHT_ULEG_LEN] = ModelerControl("Right Upper Leg Length", 1.3f, 2.5f, 0.01f, 1.5f);
	controls[RIGHT_LLEG_LEN] = ModelerControl("Right Lower Leg Length", 1.3f, 2.0f, 0.01f, 1.5f);

	controls[LEG_A] = ModelerControl("Leg Radius A", 0.45f, 0.75f, 0.01f, 0.6f);
	controls[LEG_B] = ModelerControl("Leg Radius B", 0.45f, 0.75f, 0.01f, 0.6f);
	controls[LEG_C] = ModelerControl("Leg Radius C", 0.45f, 0.75f, 0.01f, 0.6f);

	controls[LEFT_FEET_ROT] = ModelerControl("Left Feet Rotation", -90, 90, 1, 0);
	controls[RIGHT_FEET_ROT] = ModelerControl("Right Feet Rotation", -90, 90, 1, 0);

	controls[MOVE_LEFT_HAND] = ModelerControl("Move Left Hand", 0.1f, 1.8f, 0.01f, 1.0f);
	controls[MOVE_RIGHT_HAND] = ModelerControl("Move Right Hand", 0.1f, 1.8f, 0.01f, 1.0f);

	controls[LEFT_UPPER_ANGLE] = ModelerControl("Rotate Left Upper Arm", -40, 40, 1, 30);
	controls[LEFT_LOWER_ANGLE] = ModelerControl("Rotate Left Lower Arm", -90, 90, 1, 70);
	controls[RIGHT_UPPER_ANGLE] = ModelerControl("Rotate Right Upper Arm", -40, 40, 1, -17);
	controls[RIGHT_LOWER_ANGLE] = ModelerControl("Rotate Right Lower Arm", -90, 90, 1, 70);
	
	controls[LEFT_LEG_HANGLE] = ModelerControl("Rotate Left Upper Leg", -100, 20, 1, -20);
	controls[LEFT_LEG_LANGLE] = ModelerControl("Rotate Left Lower Leg", -60, 60, 1, -40);
	controls[RIGHT_LEG_HANGLE] = ModelerControl("Rotate Right Upper Leg", -100, 20, 1, -70);
	controls[RIGHT_LEG_LANGLE] = ModelerControl("Rotate Right Lower Leg", -60, 60, 1, -10);

	controls[LEFT_LEG_ZH_ROT] = ModelerControl("Rotate Left Upper Leg (2)", -100, 100, 1, 0);
	controls[LEFT_LEG_ZL_ROT] = ModelerControl("Rotate Left Lower Leg (2)", -100, 100, 1, 0);
	controls[RIGHT_LEG_ZH_ROT] = ModelerControl("Rotate Right Upper Leg (2)", -100, 100, 1, 0);
	controls[RIGHT_LEG_ZL_ROT] = ModelerControl("Rotate Right Lower Leg (2)", -100, 100, 1, 0);

	controls[LSYS_N] = ModelerControl("L-System: N", 0, 4, 1, 0);
	controls[LSYS_ANG] = ModelerControl("L-System: Angle", 30, 60, 1, 45);
	controls[LSYS_LINE] = ModelerControl("L-System: Line Length", 0.1f, 0.20f, 0.01f, 0.15f);
	controls[LSYS_LEAF] = ModelerControl("L-System: Leaf Length", 0.1f, 0.8f, 0.01f, 0.5f);
	controls[LSYS_R] = ModelerControl("L-System: Radius", 0.02f, 0.16f, 0.01f, 0.04f);

	controls[TEXTURE_LIGHT] = ModelerControl("Texture Lighting", 0, 1, 1, 1);
	controls[TEXTURE_X] = ModelerControl("Texture Scale X", 0.01f, 1.0f, 0.01f, 0.8f);
	controls[TEXTURE_Y] = ModelerControl("Texture Scale Y", 0.01f, 1.0f, 0.01f, 0.8f);
	controls[TEXTURE_Z] = ModelerControl("Texture Scale Z", 0.01f, 1.0f, 0.01f, 0.8f);
	controls[TEXTURE_N] = ModelerControl("Texture Change N", 0, 2, 1, 0);
	controls[TEXTURE_ROT] = ModelerControl("Texture Rotation", -30.0f, 30.0f, 0.01f, 0.0f);

	controls[TORUS_THICKNESS] = ModelerControl("Change Torus Thickness", 0.2f, 1.0f, 0.01f, 0.6f);
	controls[TORUS_INNER_R] = ModelerControl("Change Torus Inner Raidus", 1, 3, 0.01f, 1);
	controls[TORUS_TUBE_R] = ModelerControl("Change Torus Tube Raidus", 0.1f, 0.8f, 0.01f, 0.4f);
	//controls[TORUS_K] = ModelerControl("Change Torus K", 1, 10, 1, 1);

	controls[TORUS_R] = ModelerControl("Torus Color R", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[TORUS_G] = ModelerControl("Torus Color G", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[TORUS_B] = ModelerControl("Torus Color B", 0.0f, 1.0f, 0.01f, 0.0f);

	controls[COLOR_BODY_R] = ModelerControl("Body Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_BODY_G] = ModelerControl("Body Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_BODY_B] = ModelerControl("Body Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_HEAD_R] = ModelerControl("Head Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_HEAD_G] = ModelerControl("Head Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_HEAD_B] = ModelerControl("Head Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_CONE_R] = ModelerControl("Cone Color R", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_CONE_G] = ModelerControl("Cone Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_CONE_B] = ModelerControl("Cone Color B", 0.0f, 1.0f, 0.01f, 0.0f);

	controls[COLOR_TAIL_R] = ModelerControl("Tail Color R", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_TAIL_G] = ModelerControl("Tail Color G", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_TAIL_B] = ModelerControl("Tail Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_TAILBALL_R] = ModelerControl("Tail Ball Color R", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_TAILBALL_G] = ModelerControl("Tail Ball Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_TAILBALL_B] = ModelerControl("Tail Ball Color B", 0.0f, 1.0f, 0.01f, 0.0f);

	controls[COLOR_LSYS_R] = ModelerControl("L-System Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LSYS_G] = ModelerControl("L-System Color G", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_LSYS_B] = ModelerControl("L-System Color B", 0.0f, 1.0f, 0.01f, 0.0f);

	controls[COLOR_UARM_R] = ModelerControl("Upper Arm Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_UARM_G] = ModelerControl("Upper Arm Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_UARM_B] = ModelerControl("Upper Arm Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_LARM_R] = ModelerControl("Lower Arm Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LARM_G] = ModelerControl("Lower Arm Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LARM_B] = ModelerControl("Lower Arm Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_ARMKNOT_R] = ModelerControl("Arm Knot Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_ARMKNOT_G] = ModelerControl("Arm Knot Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_ARMKNOT_B] = ModelerControl("Arm Knot Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_HAND_R] = ModelerControl("Hand Color R", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_HAND_G] = ModelerControl("Hand Color G", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_HAND_B] = ModelerControl("Hand Color B", 0.0f, 1.0f, 0.01f, 1.0f);
	
	controls[COLOR_ULEG_R] = ModelerControl("Upper Leg Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_ULEG_G] = ModelerControl("Upper Leg Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_ULEG_B] = ModelerControl("Upper Leg Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_LLEG_R] = ModelerControl("Lower Leg Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LLEG_G] = ModelerControl("Lower Leg Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LLEG_B] = ModelerControl("Lower Leg Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_LEGUKNOT_R] = ModelerControl("Upper Leg Knot Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LEGUKNOT_G] = ModelerControl("Upper Leg Knot Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LEGUKNOT_B] = ModelerControl("Upper Leg Knot Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_LEGLKNOT_R] = ModelerControl("Lower Leg Knot Color R", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LEGLKNOT_G] = ModelerControl("Lower Leg Knot Color G", 0.0f, 1.0f, 0.01f, 0.0f);
	controls[COLOR_LEGLKNOT_B] = ModelerControl("Lower Leg Knot Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	controls[COLOR_FEET_R] = ModelerControl("Feet Color R", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_FEET_G] = ModelerControl("Feet Color G", 0.0f, 1.0f, 0.01f, 1.0f);
	controls[COLOR_FEET_B] = ModelerControl("Feet Color B", 0.0f, 1.0f, 0.01f, 1.0f);

	ModelerApplication::Instance()->Init(&createModel, controls, NUMCONTROLS);
	return ModelerApplication::Instance()->Run();
}
